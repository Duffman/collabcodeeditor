
function ClientRoom(roomName, passwordProtected) {
	this.roomName = roomName;
	this.passwordProtected = passwordProtected;
	this.isDefined = function() {
		return this.roomName != undefined;
	}
};
function Room(roomName,password) { //maybe put this in seperate script and load it into client as well as server.
	this.roomName = roomName;
	this.password = password;
	this.isActive = false;
	this.isDefined = function() {
		return this.roomName != undefined;
	}
	this.getClientRoom = function() {
		return new ClientRoom(this.roomName,this.password.length > 0);
	}
}
function transformToClientRooms(arr) {
	var clientArr = arr.map(function(r) { 
			return r.getClientRoom();
		}
	);
	return clientArr;
}
function filterUndefinedRooms(arr) {
	var newArr = [];
	for(i = 0; i < arr.length; i++){
		if (arr[i].isDefined()) {
			newArr.push(arr[i]);
		}
	}
	return newArr;
};
function getFirstUndefinedRoomIndex(arr){
	var indexFirstUndefinedRoom;
	for(i = arr.length-1; i >= 0; i--){
		if(arr[i].isDefined()) {
			indexFirstUndefinedRoom = i+1;
			break;
		}
	}
	return indexFirstUndefinedRoom;
}
function Document(documentName,contents){
	this.documentName = documentName;
	this.contents = contents;	
};