//ideeen:
//Lijst bijhouden die client linked met nickname 

var http = require('http'), 
    io = require('socket.io'),    // Let's use socket.io now
    fs = require('fs');           // Let's serve an HTML file from the file system now.
    path = require('path');       // For dealing with file paths when serving files

// Importing slightly edited Flapjax version to work on the server. Edited version by Andoni Lombide Carreton.
require('./flapjax_server.js');
// Test if it works	
console.log("Attempting to create event stream to check if Flapjax works...");
console.log(receiverE());
console.log("Event stream printed? Should work then!");

var server = http.createServer(function (request, response) {
	// Serving files is now not hard coded anymore (except for chat.html)
	// because we need to serve some JavaScript-libraries.
  	var filePath = '.' + request.url;
	if (filePath == './') filePath = './WeCode.html';
	var extname = path.extname(filePath);
	var contentType = 'text/html';
	
	//Not sure if this is necessary.
	switch (extname) {
	  case '.js':
	    contentType = 'text/javascript';
	    break;
	       // We could add other branches here, e.g. to check for CSS or other types of files
	}

	path.exists(filePath, function(exists) {
      if (exists) {
	    fs.readFile(filePath, function(error, content) {
	      if (error) {
	        response.writeHead(500);
	        response.end();
	      } else {
	        response.writeHead(200, { 'Content-Type': contentType });
	        response.end(content, 'utf-8');
	      }
	    });
	  } else {
		response.writeHead(404);
	    response.end();
	  }
    });
});

server.listen(8888);
var socket = io.listen(server);
String.prototype.replaceAt = function(index, c) {
    return this.substr(0, index) + c + this.substr(index + (c.length == 0 ? 1 : c.length));
}


//Constructors
//When transmitting rooms to the clientSide, we omit information such as the password of the room.
function ClientRoom(roomName, passwordProtected) {
	this.roomName = roomName;
	this.passwordProtected = passwordProtected;
	this.isDefined = function() {
		return this.roomName != undefined;
	}
};

function Room(roomName,password) {
	this.roomName = roomName;
	this.password = password;
	this.isActive = false;
	this.joinedUsers = new Array();
	this.isDefined = function() {
		return this.roomName != undefined;
	}
	this.getClientRoom = function() {
		var clientRoom = new ClientRoom(this.roomName,this.password.length > 0);
		return clientRoom;	
	}
}

function ActiveUser(clientId, nickname){
	this.clientId = clientId;
	this.nickname = nickname;
	this.line = 0;
	this.ch = 0;
};
//Used to keep track of the users that are in a room.
function ActiveUsersRoom(roomName){
	this.roomName = roomName;
	this.activeUsers = [];
	this.emitActiveUsers = function() {
		this.emitEventWithValueToActiveUsers('activeUsersInRoom',this.activeUsers);
	};
	this.emitEventWithValueToActiveUsers = function(eventName,value,excludingClient){
		for(var i = 0; i < this.activeUsers.length; i++){
			var cl = getClientByClientId(this.activeUsers[i].clientId)
			if(cl != excludingClient){
				cl.emit(eventName,value);
			}
		}
	};
}

function DocumentsRoom(roomName){
	this.roomName = roomName;
	this.documents = [];
	this.getDocumentByDocumentName = function(documentName){
		for(var i = 0;i < this.documents.length; i++){
			if(this.documents[i].documentName == documentName)
				return this.documents[i];
		}
	};
}
function Document(documentName){
	this.documentName = documentName;
	this.contents = "";	
	this.snapshots = [];
	this.getLine = function(lineNumber){
		var lines = this.contents.split('\n');
		return lines[lineNumber];
	};
	this.replaceLine = function(lineNumber,replacementLine){
		//debugger;
		var lines = this.contents.split('\n');
		var startOfLine = 0;
		for(var i = 0; i < lineNumber; i++){
			startOfLine = startOfLine + lines[i].length;
		};
		startOfLine = startOfLine + i;
		endOfLine = startOfLine + lines[i].length;
		this.contents = this.contents.substring(0,startOfLine) + replacementLine + this.contents.substring(endOfLine,this.contents.length);
		console.log("NEW CONTENTS: " + this.contents);
		return this.contents;
	};
	this.removeLine = function(lineNumber){
		debugger;
		var lines = this.contents.split('\n');
		var startOfLine = 0;

		for(var i = 0; i < lineNumber; i++){
			startOfLine = startOfLine + lines[i].length;
		};
		var endOfLine = startOfLine + lines[i].length;
		for(var i = startOfLine; i <= endOfLine; i++){
			this.contents = this.contents.replaceAt(startOfLine,'');
		}
	}
};

function Snapshot(snapshotName, contents){
	this.snapshotName = snapshotName;
	this.contents = contents;
}

//Object that keeps track of the votes for a certain snapshot.
function Voting(snapshot,document,roomIndex){
	this.id;
	this.votes = [];
	this.snapshot = snapshot;
	this.document = document;
	this.roomIndex = roomIndex;
	this.votingPassed;
}
function Vote(clientId){
	this.clientId = clientId;
	this.castedVote = ""; //true if client votes yes, false if client votes no.
}

//Transform array of Room-objects to ClientRoom-objects.
function transformToClientRooms(arr) {
	var clientArr = arr.map(function(r) { 
			return r.getClientRoom();
		}
	);
	return clientArr;
}

//Returns an array that only holds active and defined rooms.
function filterUndefinedRooms(arr) {
	var newArr = [];
	for(i = 0; i < arr.length; i++){
		if (arr[i].isDefined() && arr[i].isActive) {
			newArr.push(arr[i]);
		}
	}
	return newArr;
};

//Returns the index of the first room that is inactive and can thus be overwritten with an active room.
function getFirstAvailableRoomIndex(roomArr){
	for(var i = 0; i < roomArr.length; i++){
		if(!roomArr[i].isActive){
			return i;
		}
	}
	return -1;
}


function getIndexOfRoomName(arr, roomName){
	for (var i = 0; i < arr.length; i++) {
		if (arr[i].roomName == roomName)
		{
			return i;
		}
	}
}	
/*If you want to use the method insertValueE to fill nested arrays or arrays with custom objects(such as Room, Document,...)
the structure of the array you want to fill needs to have the correct structure.
To this end the expandRoomsArrays-function makes sure the structure of the arrays relating to rooms (arrays.rooms, arrays.activeUsersRooms and arrays.documentsRooms)
always has a structure that supports the creation of a new room. 
*/
function expandRoomsArrays(_) {
	var inactiveRoomIndex;
	inactiveRoomIndex = getFirstAvailableRoomIndex(arrays.rooms);
	if(inactiveRoomIndex == -1){
		arrays.rooms.push(new Room());
		arrays.activeUsersRooms.push(new ActiveUsersRoom());
		arrays.documentsRooms.push(new DocumentsRoom());
	}else{
		arrays.rooms[inactiveRoomIndex] = new Room();
		arrays.activeUsersRooms[inactiveRoomIndex] = new ActiveUsersRoom();
		arrays.documentsRooms[inactiveRoomIndex] = new DocumentsRoom();
	}
}	



function getClientByClientId(clientId){
	for(var i = 0; i < clients.length; i++){
		if(clients[i].id == clientId)
			return clients[i];
	}
};

var arrays = new Object();	
arrays.rooms = [new Room()];
arrays.activeUsersRooms = [new ActiveUsersRoom()];
arrays.documentsRooms = [new DocumentsRoom()];
arrays.savedDocuments = [new Document()];
var clients = [];
var votings = [];

var roomsE = receiverE();
var activeUsersRoomsE = receiverE();
var documentsRoomsE = receiverE();
var savedDocumentsE = receiverE();

insertValueE(roomsE,arrays,"rooms");
insertValueE(activeUsersRoomsE,arrays,"activeUsersRooms");
insertValueE(documentsRoomsE,arrays,"documentsRooms");
insertValueE(savedDocumentsE,arrays,"savedDocuments");
roomsE.mapE(expandRoomsArrays);//everytime a new room is created, we will make sure that the structure of the different arrays is ready to accept another new room.

/*var saveSnapshotTimerB = timerB(100);
var test;
function test2(beh){//not getting this function to fire every 100 milisecs..
	test = beh/100;
	console.log(beh);
}*/

var saveSnapshotE = timerE(15000);
saveSnapshotE.mapE(function(timerVal) {
	//loop through all documents and make a snapshot if the content has changed since the last snapshot.
	for(var i = 0; i < arrays.documentsRooms.length; i++)
		for(var j = 0; j < arrays.documentsRooms[i].documents.length; j++){
			var doc = arrays.documentsRooms[i].documents[j];
			var snapshotName = "snapshot" + doc.documentName + arrays.documentsRooms[i].roomName + "_" + timerVal
			if(doc.snapshots[doc.snapshots.length-1] != undefined){
				if(doc.snapshots[doc.snapshots.length-1].contents != doc.contents){
					doc.snapshots.push(new Snapshot(snapshotName, arrays.documentsRooms[i].documents[j].contents));
				}
			}else{
					doc.snapshots.push(new Snapshot(snapshotName, arrays.documentsRooms[i].documents[j].contents));
			}
		}
	});

socket.on('connection',function(client) {
	var nickname;
	var previousRoomIndex;
	var currentRoomIndex;
	var activeUserAddedE = receiverE();
	var activeUserRemovedE = receiverE();
	var documentAddedE = receiverE();
	insertValueE(mergeE(activeUserAddedE,activeUserRemovedE),arrays,"activeUsersRooms");
	insertValueE(documentAddedE,arrays,"documentsRooms");
	var activeUserAddedB = activeUserAddedE.startsWith(arrays.activeUsersRooms);
	activeUserRemovedE.mapE(activeUserRemoved);
	activeUserAddedB.liftB(activeUserAdded);
	client.emit('clientId',client.id);

	//function definitions
	//remove user from activeUsersRooms and let the other clients of that room know.
	//if room is empty it will be set to inactive.
	function activeUserRemoved(activeUsersRooms){
		if(previousRoomIndex != undefined){
			if(activeUsersRooms[previousRoomIndex].activeUsers.length > 0){
				activeUsersRooms[previousRoomIndex].emitActiveUsers();
			}else {
				setRoomInactiveByIndex(previousRoomIndex);
			}
		}
	}
	//We rather set rooms to inactive than delete them to make sure that the previousRoomIndex and currentRoomIndex of the other users stays intact.
	//These inactive rooms will be overwritten when a new room is created.
	function setRoomInactiveByIndex(index){
		var newRoomsArr = [];
		for(var i = 0; i < arrays.rooms.length; i++){
			newRoomsArr.push(arrays.rooms[i]);
		}
		newRoomsArr[index].isActive = false;
		roomsE.sendEvent(newRoomsArr);
		for(var i = 0; i < clients.length;i++){
			clients[i].emit('roomRemoved',newRoomsArr[index].getClientRoom());
		}
	};
	
	//add user to room and let the other users of that room know.
	function activeUserAdded(activeUsersRooms){
		if(currentRoomIndex != undefined && activeUsersRooms[currentRoomIndex] != undefined){
			activeUsersRooms[currentRoomIndex].emitActiveUsers();
			client.emit('documentsList',arrays.documentsRooms[currentRoomIndex].documents);//emitting seperately because we only want to emit the new
		}
	}
	
	function leaveRoom(arr){
		previousRoomIndex = currentRoomIndex;
		if(previousRoomIndex != undefined){
			for(var i = 0; i < arr[previousRoomIndex].activeUsers.length ; i++) {
				if(arr[previousRoomIndex].activeUsers[i].clientId == client.id) {
					arr[previousRoomIndex].activeUsers.splice(i,1);
					arrays.activeUsersRooms[previousRoomIndex].activeUsers.splice(i,1);
					i--;
				}
			}
		}
		activeUserRemovedE.sendEvent(arr);	
	};
	function deepCopyOfActiveUsersRooms(){
		var arr = new Array();
		var previousRoomIndex = currentRoomIndex;
		for(var i = 0; i < arrays.activeUsersRooms.length; i++)
		{
			arr.push(new ActiveUsersRoom(arrays.activeUsersRooms[i].roomName));
			for(var j = 0; j < arrays.activeUsersRooms[i].activeUsers.length; j++){
				arr[i].activeUsers.push(arrays.activeUsersRooms[i].activeUsers[j]);
			}
		}
		return arr;
	};	
	function commitRevert(votingId){
		var voting = votings[votingId];
		voting.document.contents = voting.snapshot.contents;
		arrays.activeUsersRooms[voting.roomIndex].emitEventWithValueToActiveUsers('updatedBySnapshot',[voting.document.documentName, voting.snapshot.contents]);													
	}
	clients.push(client);
	console.log("roomslist : " + arrays.rooms);
	client.emit('roomsList',transformToClientRooms(filterUndefinedRooms(arrays.rooms)));
	//change nickname
	client.on('nickname', function(nn) {
		nickname = nn;
	});
	
	function setVote(votingId,castVote){
		debugger;
		var voting = votings[votingId];
		for(var i = 0; i < voting.votes.length;i++){
			if(voting.votes[i].clientId == client.id){
				voting.votes[i].castedVote = castVote;
			}
		}
		if(castVote == false && voting.votingPassed != false){
			voting.votingPassed = false;
			arrays.activeUsersRooms[voting.roomIndex].emitEventWithValueToActiveUsers('voteFailed',voting.document.documentName);													

		}
		if(voting.votingPassed == undefined){
			var everyoneVotedYes = true;
			for(var i = 0; i <voting.votes.length;i++){
				if(voting.votes[i].castedVote != true){//explicitly comparing to false because it might be undefined;
					everyoneVotedYes = false;
					break;
				}
			}
			if(everyoneVotedYes){
				voting.votingPassed = true;
				commitRevert(votingId);
			}
		}
	}	
	
	
	client.on('newRoom', function(room) {
		var roomName = room.roomName;
		var password = room.password;
		var newRoomsArr = [];
		var newActiveUsersRoomsArr = [];
		var newDocumentsRooms = [];
		console.log("new room: " + roomName);
		
		if(getIndexOfRoomName(arrays.rooms,roomName) == undefined) {//check whether a room with this name already exists.
			for(var i = 0; i < arrays.rooms.length; i++){
				newRoomsArr.push(arrays.rooms[i]);
				newDocumentsRooms.push(arrays.documentsRooms[i]);
			}
			newActiveUsersRoomsArr = deepCopyOfActiveUsersRooms();
			var availableRoomIndex = getFirstAvailableRoomIndex(newRoomsArr);
			newRoomsArr[availableRoomIndex] = new Room(roomName,password)//making a new room instead of the one being passed as an argument because for some reason the methods of an object tend to disappear when they are transmitted between server and client.
			newRoomsArr[availableRoomIndex].isActive = true;
			newActiveUsersRoomsArr[availableRoomIndex] = new ActiveUsersRoom(roomName);
			newDocumentsRooms[availableRoomIndex] = new DocumentsRoom(roomName);

			roomsE.sendEvent(newRoomsArr);
			activeUsersRoomsE.sendEvent(newActiveUsersRoomsArr);
			documentsRoomsE.sendEvent(newDocumentsRooms);
			for (i = 0; i < clients.length; i++) {				
				clients[i].emit('newRoom',[newRoomsArr[availableRoomIndex].getClientRoom(),client.id]);
			}
		} else {
			client.emit('roomExists',roomName);
		}
	});
	
	client.on('selectRoom', function(roomName) {
		previousRoomIndex = currentRoomIndex;
		var newRoomIndex;
		var arr = deepCopyOfActiveUsersRooms();
		for(var i = 0; i < arr.length; i++){
			if(arr[i].roomName == roomName){
				newRoomIndex = i;
			}
		}
		//remove user from previous room if any.
		leaveRoom(arr);
		arrays.activeUsersRooms[newRoomIndex].activeUsers.push(new ActiveUser());//need to have an "empty" object ready to add new ActiveUser. Doesn't matter what's inside the object. just needs to be there...
		arr[newRoomIndex].activeUsers.push(new ActiveUser(client.id,nickname));
		currentRoomIndex = newRoomIndex;
		activeUserAddedE.sendEvent(arr);
	});

	//removes user from the room it is in and removes its client-entry from the server so it doesn't get any messages anymore.
	client.on('leaveApplication',function(){
		var arr = deepCopyOfActiveUsersRooms();
		leaveRoom(arr);
		for(var i = 0; i < clients.length; i++){
			if(clients[i].id == client.id){
				clients.splice(i,1);
			}
		}
	});
	
	client.on('newMessage',function(msg){
		if(currentRoomIndex != undefined && arrays.activeUsersRooms[currentRoomIndex] != undefined && msg.length > 0 ){
			var message = "<" + nickname + ">: " + msg;
			arrays.activeUsersRooms[currentRoomIndex].emitEventWithValueToActiveUsers('newMessage',message);
		}
	});
	
	//creates a text-editing session (extra tab on the client-side)
	client.on('newDocument',function(doc){
		var docNameExists = false;
		var documentName = doc.documentName;
		if(currentRoomIndex != undefined){
			for(var i = 0; i < arrays.documentsRooms[currentRoomIndex].documents.length; i++){
				if(arrays.documentsRooms[currentRoomIndex].documents[i].documentName == documentName){
					docNameExists = true;
					break;
				}
			}
			if(!docNameExists && documentName.length > 0){
				//var document = new Document(documentName);
				var newDocumentsRooms = [];
				for(var i = 0; i < arrays.documentsRooms.length; i++){
					newDocumentsRooms.push(new DocumentsRoom(arrays.documentsRooms[i].roomName));
					for(var j = 0; j < arrays.documentsRooms[i].documents.length; j++){
						newDocumentsRooms[i].documents.push(arrays.documentsRooms[i].documents[j]);
					}
				}
				
				arrays.documentsRooms[currentRoomIndex].documents.push(new Document());
				newDocumentsRooms[currentRoomIndex].documents.push(doc);
				
				documentAddedE.sendEvent(newDocumentsRooms);
				arrays.activeUsersRooms[currentRoomIndex].emitEventWithValueToActiveUsers('newDocumentCreated',doc);
			}else {
				client.emit('documentNotCreated',docNameExists);
			}
		}
	});
	
	client.on('cursorActivity',function(lineChar){
		var line = lineChar[0];
		var ch = lineChar[1];
		var updatedUserIndex;
		var currentActiveUsersRoom = arrays.activeUsersRooms[currentRoomIndex]
		for(var i = 0; i < currentActiveUsersRoom.activeUsers.length ; i++) {
				if(currentActiveUsersRoom.activeUsers[i].clientId == client.id) {
					currentActiveUsersRoom.activeUsers[i].line = line;
					currentActiveUsersRoom.activeUsers[i].ch = ch;
					updatedUserIndex = i;
					break;
				}
		}
		currentActiveUsersRoom.emitEventWithValueToActiveUsers('updatedActiveUser',currentActiveUsersRoom.activeUsers[updatedUserIndex]);

	});
	//Save document on the server
	client.on('saveDocument',function(doc){
		var arr = new Array();
		for(var i = 0; i < arrays.savedDocuments.length; i++){
			arr.push(arrays.savedDocuments[i]);
		}
		arr[arr.length-1] = doc;
		savedDocumentsE.sendEvent(arr);
	});
	
	client.on('inspectSavedDocuments',function(_) {
		console.log("inspecting savedDocs");
		client.emit('savedDocumentsList',arrays.savedDocuments);
	});
	
	client.on('snapshotsList',function(documentName){
		var snapshots = arrays.documentsRooms[currentRoomIndex].getDocumentByDocumentName(documentName).snapshots;
		client.emit('snapshotsList',snapshots);
	});
	
	//Emits a message to prompt the users to vote.
	client.on('castVote',function(args){
		var snapshotName = args[0];
		var selectedDocumentName = args[1];
		var snapshot;
		var doc;
		var roomIndex;
		//usually, the user who is asking for the vote will still be in the same room and document so we look for the snapshot there first.
		var currentDocument = arrays.documentsRooms[currentRoomIndex].getDocumentByDocumentName(selectedDocumentName);
		if(currentDocument != undefined){
			for(var i = 0; i < currentDocument.snapshots.length; i++){
				if(currentDocument.snapshots[i].snapshotName == snapshotName){
					snapshot = currentDocument.snapshots[i];
					doc = currentDocument;
					roomIndex = currentRoomIndex;
					break;
				}
			}
		}
		//if not found there: search through all of the snapshots
		if (snapshot == undefined){
			for(var i = 0; i < arrays.documentsRooms.length; i++)
				for(var j = 0; j < arrays.documentsRooms[i].documents.length; j++)
					for(var k = 0; k < arrays.documentsRooms[i].documents[j].snapshots.length; k++){
						if(arrays.documentsRooms[i].documents[j].snapshots[k].snapshotName == snapshotName){
							snapshot = arrays.documentsRooms[i].documents[j].snapshots[k];
							doc = arrays.documentsRooms[i].documents[j];
							roomIndex = i;
							break;
						}
					}
		}
		//Initiate the vote
		var voting = new Voting(snapshot,doc,roomIndex);
		votings.push(voting);
		voting.id = votings.length-1;
		for(var i = 0; i < clients.length; i++){
			clients[i].emit('voteCast',voting);
		}
	});
	
	//This way we know from which users we expect a voting answer. If all of the clients that have emitted voteAck have voted yes, the document's content will be the same content as the snapshot.
	client.on('voteAck',function(votingId){
		votings[votingId].votes.push(new Vote(client.id));
	});
	
	client.on('voteYes',function(votingId){
		setVote(votingId,true);
	});
	client.on('voteNo',function(votingId){
		setVote(votingId,false);
	});

	
	function insertNewLine(doc,changeObj){
		var lineOnServer = doc.getLine(changeObj.from.line);
		var newContentBeginningLine = lineOnServer.substring(0,changeObj.from.ch) + '\n' + lineOnServer.substring(changeObj.from.ch ,lineOnServer.length);
		doc.replaceLine(changeObj.from.line,newContentBeginningLine);
		arrays.activeUsersRooms[currentRoomIndex].emitEventWithValueToActiveUsers('updatedLine',[changeObj.from.line,newContentBeginningLine,doc.documentName],client);													
	}
	
	//This function needs refactoring and abstractions but due to bad planning on my behalf I didn't get to it.
	//The application supports all operations except for when they involve selection.
	//Deleting selection on one line does work, replace selection over multiple lines by one character (not pasting) works.
	client.on('editedOneLine',function(args) {
		var documentName = args[0];
		var changeObj = args[1];
		var firstNewLineContent = args[2];//how it looks like on server
		var lastNewLineContent = args[3];
		var doc = arrays.documentsRooms[currentRoomIndex].getDocumentByDocumentName(documentName);

		switch(changeObj.origin){
			case "input": {
				var lineOnServer = doc.getLine(changeObj.from.line);
				if(changeObj.text.length == 1){
					var lineOnClientWithoutReplacement = firstNewLineContent.substring(0,changeObj.from.ch).concat(firstNewLineContent.substring(changeObj.from.ch + changeObj.text[0].length,firstNewLineContent.length));
					var lineOnServerWithoutRemovedChars = lineOnServer.substring(0,changeObj.from.ch).concat(lineOnServer.substring(changeObj.to.ch,lineOnServer.length));
					if(changeObj.from.line == changeObj.to.line){// covers normal typing, cutting from one line, replacing selection on one line.
						if(lineOnServerWithoutRemovedChars == lineOnClientWithoutReplacement && lineOnServer != firstNewLineContent){
							doc.replaceLine(changeObj.from.line,firstNewLineContent);
							arrays.activeUsersRooms[currentRoomIndex].emitEventWithValueToActiveUsers('updatedLine',[changeObj.from.line,firstNewLineContent,doc.documentName]);
						}else{
							client.emit('updatedLine',[changeObj.from.line,doc.getLine(changeObj.from.line),doc.documentName]);
						}
					}else{
						var firstPartOnServer = lineOnServer.substring(0,changeObj.from.ch);
						lineOnServer = doc.getLine(changeObj.to.line);
						var secondPartOnServer = lineOnServer.substring(changeObj.to.ch,lineOnServer.length);
						var lineOnServerWithoutRemovedChars = firstPartOnServer + secondPartOnServer;
						if(lineOnServerWithoutRemovedChars == lineOnClientWithoutReplacement){
							doc.replaceLine(changeObj.from.line,firstNewLineContent);
							arrays.activeUsersRooms[currentRoomIndex].emitEventWithValueToActiveUsers('updatedLine',[changeObj.from.line,firstNewLineContent,doc.documentName]);
						}else{
							client.emit('updatedLine',[changeObj.from.line,doc.getLine(changeObj.from.line),doc.documentName]);					
						}
						for(var i = changeObj.from.line + 1; i <= changeObj.to.line; i++){
	
							doc.removeLine(changeObj.from.line + 1);
							arrays.activeUsersRooms[currentRoomIndex].emitEventWithValueToActiveUsers('removedLine',[doc.documentName,changeObj.from.line + 1],client);																		
						}
					}
				}else{
					if(changeObj.from.line == changeObj.to.line){
						insertNewLine(doc,changeObj);
					}//else tak-nog voorzien: multiple lines vervangen door 1 enter.
				}
			}break;	
			case "paste": {
				debugger;
				var lineOnServer = doc.getLine(changeObj.from.line);
				var lineOnClientWithoutPaste = firstNewLineContent.substring(0,changeObj.from.ch).concat(firstNewLineContent.substring(changeObj.from.ch + changeObj.text[0].length,firstNewLineContent.length));
				if(changeObj.text.length == 1){
					if(lineOnServer == lineOnClientWithoutPaste){//else multiple lines replacen door 1 lijn of copy pasta
						doc.replaceLine(changeObj.from.line,firstNewLineContent);
						arrays.activeUsersRooms[currentRoomIndex].emitEventWithValueToActiveUsers('updatedLine',[changeObj.from.line,firstNewLineContent,doc.documentName]);						
					}else{
						client.emit('updatedLine',[changeObj.from.line,doc.getLine(changeObj.from.line),doc.documentName]);				
					}
				}else{//pasting multiple lines at once
					firstLineOnClientWithoutPaste = firstNewLineContent.substring(0,changeObj.from.ch);
					lastLineOnClientWithoutPaste = lastNewLineContent.substring(changeObj.text[changeObj.text.length-1].length,lastNewLineContent.length);
					var lineOnClientWithoutPaste = firstLineOnClientWithoutPaste + lastLineOnClientWithoutPaste;
					if(lineOnClientWithoutPaste == lineOnServer){
						var accumulatedNewLine = firstLineOnClientWithoutPaste;
						for(var i = 0; i < changeObj.text.length -1; i++)
						{
							accumulatedNewLine = accumulatedNewLine.concat(changeObj.text[i]).concat('\n');
						}
						accumulatedNewLine = accumulatedNewLine.concat(changeObj.text[changeObj.text.length-1]).concat(lastLineOnClientWithoutPaste);
						doc.replaceLine(changeObj.from.line,accumulatedNewLine);
						arrays.activeUsersRooms[currentRoomIndex].emitEventWithValueToActiveUsers('updatedLine',[changeObj.from.line,accumulatedNewLine,doc.documentName],client);												
					}
				}
			}break;
			case "delete": {
				var lineOnServer = doc.getLine(changeObj.from.line);
				lineOnServerWithoutRemovedChars = lineOnServer.substring(0,changeObj.from.ch).concat(lineOnServer.substring(changeObj.to.ch,lineOnServer.length));
				if(lineOnServerWithoutRemovedChars == firstNewLineContent){
					doc.replaceLine(changeObj.from.line,firstNewLineContent);
					arrays.activeUsersRooms[currentRoomIndex].emitEventWithValueToActiveUsers('updatedLine',[changeObj.from.line,firstNewLineContent,doc.documentName]);											
				}else
					client.emit('updatedLine',[changeObj.from.line,doc.getLine(changeObj.from.line),doc.documentName]);
			}break;
		}
	});
	client.on('debug', function() {
		debugger;
	});
	
	client.on('uncaughtException', function(err) {
		console.log("uncaughtException");
	});
	client.on('SIGTERM', function() {
		console.log("SIGTERM");
	});		
});




console.log('Server running at http://localhost:8888/');
